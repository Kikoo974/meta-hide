﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour {
    Arbalete arbNumber;
    private int shield;
    private GameObject player;
	// Use this for initialization
	void Start () {
        arbNumber = GameObject.Find("arbalete").GetComponent<Arbalete>();
        shield = arbNumber.shieldNumber;
        player = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnCollisionEnter2D(Collision2D coll)
    {
        if(coll.gameObject.tag== "Player" && player.GetComponent<SpriteRenderer>().sprite == GameObject.Find("Inventory").GetComponent<InventoryScript>().transformationSprites[shield])
        {
            gameObject.tag = "Untagged";
        }
        if (coll.gameObject.tag == "sol")
            Destroy(gameObject, 1f);
    }
}
