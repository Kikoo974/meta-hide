﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Corde : MonoBehaviour {
    [SerializeField] GameObject accroche;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.name == "flame")
        {
            accroche.GetComponent<Rigidbody2D>().isKinematic = false;
            Destroy(this.gameObject);
        }
    }
}
