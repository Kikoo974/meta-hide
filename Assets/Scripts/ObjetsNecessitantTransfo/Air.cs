﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Air : MonoBehaviour {
    [SerializeField] int FlyNumber;
    [SerializeField] float AirForce = 1f;
    private bool coroutine;
    private GameObject player;
    Rigidbody2D rigid;
	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
        rigid = player.GetComponent<Rigidbody2D>();
        coroutine = false;

    }
	
	// Update is called once per frame
	void Update () {

	}
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "Player"&&!coroutine)
        {
            coroutine = true;
            StartCoroutine(Fly());
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            coroutine = false;
        }
    }
    
    IEnumerator Fly()
    {
        
        while (true)
        {
            if (player.GetComponent<SpriteRenderer>().sprite == GameObject.Find("Inventory").GetComponent<InventoryScript>().transformationSprites[FlyNumber]&&coroutine)
            {
                player.GetComponent<PlayerController>().canMove = true;
                rigid.velocity = new Vector2(0, 0);
                rigid.isKinematic = true;
                rigid.useFullKinematicContacts = true;
                player.transform.Translate(Vector2.up * AirForce);
                yield return new WaitForFixedUpdate();
            }
            else if(coroutine)
            {
                player.transform.Translate(Vector2.up * 0);
                player.GetComponent<Rigidbody2D>().isKinematic = false;
                yield return new WaitForFixedUpdate();
            }
            else
            {
                player.transform.Translate(Vector2.up * 0);
                player.GetComponent<Rigidbody2D>().isKinematic = false;
                break;
            }
        }
        /*while (player.GetComponent<SpriteRenderer>().sprite == GameObject.Find("Inventory").GetComponent<InventoryScript>().transformationSprites[FlyNumber]&&coroutine)
        {
            if (player.GetComponent<SpriteRenderer>().sprite == GameObject.Find("Inventory").GetComponent<InventoryScript>().transformationSprites[FlyNumber])
            {
                if (stop)
                {
                    rigid.velocity = new Vector2(1, 1);
                    rigid.isKinematic = true;
                    rigid.useFullKinematicContacts = true;
                    stop = false;
                }
                player.transform.Translate(Vector2.up * AirForce);
                yield return new WaitForEndOfFrame();
            }
            else
            {
                player.transform.Translate(Vector2.up * 0);
                player.GetComponent<Rigidbody2D>().isKinematic = false;
                yield return new WaitForFixedUpdate();
            }
        }
        player.transform.Translate(Vector2.up * 0);
        player.GetComponent<Rigidbody2D>().isKinematic = false;*/
    }

}
