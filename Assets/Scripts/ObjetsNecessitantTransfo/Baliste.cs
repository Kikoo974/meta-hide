﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Baliste : MonoBehaviour {
    [SerializeField] int arrowNumber;
    [SerializeField] Transform arrowPos;
    [SerializeField] float flyForce =700f;
    [SerializeField] GameObject F;
    private bool baliste, posBow, waintingShoot;
    private GameObject player;
    Animator animBow;
  
    // Use this for initialization
    void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
        animBow = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
        
		if(baliste && 
           Input.GetKeyDown("f") && 
           player.GetComponent<SpriteRenderer>().sprite == GameObject.Find("Inventory").GetComponent<InventoryScript>().transformationSprites[arrowNumber])
        {
            StartCoroutine("Fire");
        }
        if(posBow)
        {
            player.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            player.transform.position = arrowPos.position;
        }
        
	}
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            baliste = true;
            F.SetActive(true);
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            baliste = false;
            F.SetActive(false);
        }
    }
    IEnumerator Fire()
    {
        PlayerController pc = player.GetComponent<PlayerController>();
        baliste = false;
        if (gameObject.transform.localRotation.y != 0)
            player.transform.rotation = Quaternion.Euler(0, 180, 0);
        else
            player.transform.rotation = Quaternion.Euler(0, 0, 0);
        pc.canMove = false;
        pc.launching = true;
        posBow = true;
        animBow.SetTrigger("fire");
        yield return new WaitForSeconds(0.2f);
        posBow = false;
        Rigidbody2D playerRigid = player.GetComponent<Rigidbody2D>();
        if (gameObject.transform.localRotation.y != 0)
        {
            playerRigid.AddForce(Vector2.up * flyForce);
            playerRigid.AddForce(Vector2.left * flyForce);
        }
        else
            playerRigid.AddForce(Vector2.one * flyForce);


    }
}
