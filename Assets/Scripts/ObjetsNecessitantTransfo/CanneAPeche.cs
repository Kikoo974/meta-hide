﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanneAPeche : MonoBehaviour {
    private bool fishing, goFish, active;

    private Vector3 playerPos, canneSize;
    [SerializeField] float hauteurYMAX;
    [SerializeField]int canneNumber;
    [SerializeField] Transform hamesson, posMax;
    [SerializeField] GameObject player, taunter, upArrow, downArrow, F;
   
    PlayerController pc;
    Rigidbody2D p_rigidbody2D;
    // Use this for initialization
    void Start () {
        canneSize = gameObject.transform.localScale;
        pc = player.GetComponent<PlayerController>();
        p_rigidbody2D = player.GetComponent<Rigidbody2D>();
    }
	
	// Update is called once per frame
	void Update () {

        //Test si le player est transformé
        if (player.GetComponent<SpriteRenderer>().sprite == GameObject.Find("Inventory").GetComponent<InventoryScript>().transformationSprites[canneNumber])
        {
            if (fishing)
            {
                if (Input.GetButton("VerticalBas") && player.transform.position.y>=posMax.position.y) //Descendre
                {
                    F.SetActive(false);
                    transform.localScale = new Vector2(transform.localScale.x, transform.localScale.y * (1 + Time.deltaTime));
                    downArrow.transform.localScale = new Vector2(0.14f, 0.2f);
                    upArrow.transform.localScale = new Vector2(0.08f, 0.09f);

                }
                else if (Input.GetButton("VerticalHaut") &&  player.transform.position.y <= hauteurYMAX)  //Monter
                {
                    F.SetActive(false);
                    transform.localScale = new Vector2(transform.localScale.x, transform.localScale.y / (1 + Time.deltaTime));
                    upArrow.transform.localScale = new Vector2(0.14f, 0.2f);
                    downArrow.transform.localScale = new Vector2(0.08f, 0.09f);
                }
                else if (Input.GetKeyDown("f"))
                {
                    goFish = !goFish;
                    if (!goFish)             //Remet le player a la position à partir de laqulle il est renter dans le trigger
                    {
                        active = false;
                        p_rigidbody2D.isKinematic = false;
                        player.transform.position = playerPos;
                        gameObject.transform.localScale = canneSize;
                        pc.canMove = true;
                    }


                }
                if (goFish)                 //permet d'activer le script
                {
                    active = true;
                    p_rigidbody2D.isKinematic = true;
                    player.transform.position = hamesson.position;
                    pc.canMove = false;
                }

            }
        }
        else
        {
            if(fishing && active)
            {
                
                goFish = false;
                p_rigidbody2D.isKinematic = false;
                player.transform.position = playerPos;
                gameObject.transform.localScale = canneSize;
                pc.canMove = true;
                active = false;
            }
        }
            
       


    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            //  playerPos = player.transform.position;
            fishing = true;
            upArrow.SetActive(true);
            downArrow.SetActive(true);
            F.SetActive(true);
        }
    }
    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            playerPos = player.transform.position;
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            F.SetActive(false);
            fishing = false;
            upArrow.SetActive(false);
            downArrow.SetActive(false);
        }
    }
}
