﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CageAndKey : MonoBehaviour {
    [SerializeField] int total;   
    [SerializeField] GameObject key;
    [SerializeField] Sprite open;
    private int numberOn = 0;
    GameObject camera;
    // Use this for initialization
    void Start () {
        camera = GameObject.Find("Main Camera");
    }
	
	// Update is called once per frame
	void Update () {
       
		
	}
    public void GetNumber()
    {
        numberOn += 1;
        if (numberOn >= total)
        {
            key.GetComponent<Rigidbody2D>().isKinematic = false;
            gameObject.GetComponent<SpriteRenderer>().sprite = open;
            StartCoroutine("Camera");
        }
    }
    IEnumerator Camera()
    {
        camera.GetComponent<CameraFollowerStory>().enabled = false;
        camera.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, -10);
        yield return new WaitForSeconds(1f);
        camera.GetComponent<CameraFollowerStory>().enabled = true;
    }
}
