﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PorteFermer : MonoBehaviour {
    [SerializeField] int keyNumber;
    [SerializeField] Sprite openSprite, Z;
    [SerializeField] GameObject child, F, Serrure;
    
    private bool door, open;
    private GameObject player;
	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown("z"))
        {
            if(player.GetComponent<SpriteRenderer>().sprite == GameObject.Find("Inventory").GetComponent<InventoryScript>().transformationSprites[keyNumber] && 
               door)
            {
                child.SetActive(true);
                Serrure.SetActive(false);
                gameObject.GetComponent<SpriteRenderer>().sprite = openSprite;
                F.GetComponent<SpriteRenderer>().sprite = Z;
            }
           

        }
	}
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            door = true;
            F.SetActive(true);
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            door = false;
            F.SetActive(false);
        }
    }

}
