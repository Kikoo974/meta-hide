﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorcheEteinte : MonoBehaviour {
    [SerializeField] GameObject torcheAlumme, objetUse;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag =="flame" )
        {
            torcheAlumme.SetActive(true);
            if (gameObject.tag != "MovingObject")
            {
                objetUse.GetComponent<CageAndKey>().GetNumber();
                Destroy(this.gameObject);
            }
        }
    }
}
