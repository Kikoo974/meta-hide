﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossRunner : MonoBehaviour {
    [SerializeField] float speed = 3;
    public bool firstStop = false;
    InventoryScript inventoryScript;
    private Rigidbody2D m_rigidbody2D;

    void Start () {
        inventoryScript = GameObject.Find("Inventory").GetComponent<InventoryScript>();
        m_rigidbody2D = GetComponent<Rigidbody2D>();
        m_rigidbody2D.velocity = new Vector2(2, 0);
    }
	
	
	void Update () {
        
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            m_rigidbody2D.velocity = new Vector2(0, 0);
            inventoryScript.GameOver();
        }
        if (other.gameObject.name == "spikeStopBoss" && !firstStop) 
        {
            firstStop = true;
            m_rigidbody2D.velocity = new Vector2(0, 0);
        }
    }
}
