﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CameraFollower : MonoBehaviour {

    private GameObject player;
    [SerializeField] float yOffSet; 
    [SerializeField] float speed;
    [SerializeField] GameObject pos1;
    [SerializeField] GameObject pos2;
    public bool libre = false;
    // Use this for initialization
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        if (SceneManager.GetActiveScene().name != "Boss1" && SceneManager.GetActiveScene().name != "Boss")
        {
            
            if (!libre)
            {
                if(SceneManager.GetActiveScene().name != "Level 1vs1")
                    player.GetComponent<PlayerController>().enabled = true;

                transform.position = new Vector3(player.transform.position.x, yOffSet, transform.position.z);
            }
            else
            {
                if (SceneManager.GetActiveScene().name != "Level 1vs1")
                    player.GetComponent<PlayerController>().enabled = false;
                GetComponent<Rigidbody2D>().velocity = new Vector2(Input.GetAxis("Horizontal") * speed, GetComponent<Rigidbody2D>().velocity.y);
            }
        }
        else
        {
            transform.position = new Vector3(transform.position.x, player.transform.position.y + yOffSet, transform.position.z);
        }  
        if (Input.GetKeyDown("a"))
        {
            libre = !libre;
        }
        //GetComponent.velocity = new Vector2(x * speed, m_rigidbody2D.velocity.y);


    }
    public void GetYOffSet(float newY){
        yOffSet=newY;
    }
}

