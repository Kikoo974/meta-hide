﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraWingBoss : MonoBehaviour {

	private Transform player;
	public bool Follow;
	[SerializeField] float yOffSet; 
	public int FlyNumber;
	void Start () 
	{
		Follow = true;
		player = GameObject.Find("player").GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Follow)
		{
			transform.position = new Vector3(transform.position.x, player.transform.position.y + yOffSet, transform.position.z);
		}
		else
		{
			transform.position = new Vector3(transform.position.x,transform.position.y, transform.position.z);
		}
	}
}
