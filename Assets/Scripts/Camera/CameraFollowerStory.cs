﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowerStory : MonoBehaviour {

	private GameObject player;
    [SerializeField] float yOffSet, speed, xMin, xMax, yMin, yMax;
    [SerializeField] GameObject[] cachettes;
    [SerializeField] int numberOfCachettes; 
    bool libre,cutscene;
	private PlayerController playerControl;
    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
		playerControl = GameObject.Find("player").GetComponent<PlayerController>();
    	libre = false;
		cutscene =false;
    }

    // Update is called once per frame
    void Update()
    { 
		if(!cutscene){
			if(Input.GetKeyDown("a")){
                libre = !libre;
                playerControl.SetFreeCam(libre);
                SetCachetteActif();
                player.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                /*if (!libre){
					libre=true;
					playerControl.SetFreeCam(libre);
					//feedback changement de mode
				}else{
					libre=false;
					playerControl.SetFreeCam(libre);
				}*/
			}else if(!libre){
            	transform.position = new Vector3(
					Mathf.Max(xMin,Mathf.Min(xMax,player.transform.position.x)), 
					yOffSet, 
					transform.position.z);
        	}else{
            	transform.position = new Vector3(
					Mathf.Max(xMin,Mathf.Min(xMax,transform.position.x+Input.GetAxis("Horizontal") * speed)), 
					Mathf.Max(yMin,Mathf.Min(yMax,transform.position.y+Input.GetAxis("Vertical") * speed)),
					transform.position.z);
        	}
		}
        
        //GetComponent.velocity = new Vector2(x * speed, m_rigidbody2D.velocity.y);
    }
    public void SetYOffSet(float newY){
        yOffSet=newY;
    }
	public void MoveCameraToward(GameObject target){
		cutscene=true;
		StartCoroutine(MoveCamera(target));
	}
    private void SetCachetteActif()
    {
       for(int i=0; i< numberOfCachettes; i++)
        {
            cachettes[i].SetActive(libre);
        }
    }
	IEnumerator MoveCamera(GameObject target){
		player.SetActive(false);
        player.transform.position = target.transform.position+new Vector3(0,-1f,0);
		while(true){
			if(transform.position!=new Vector3(
						Mathf.Max(xMin,Mathf.Min(xMax,target.transform.position.x)), 
						yOffSet,
						transform.position.z)){
				transform.position= Vector3.MoveTowards(
					transform.position,
					new Vector3(
						Mathf.Max(xMin,Mathf.Min(xMax,target.transform.position.x)), 
						yOffSet,
						transform.position.z),
					15*Time.deltaTime);
				yield return new WaitForFixedUpdate();
			}else{
				player.SetActive(true);
				cutscene=false;
				break;
			}
		}
	}
}
