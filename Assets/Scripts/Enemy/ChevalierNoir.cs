﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ChevalierNoir : MonoBehaviour {
    [SerializeField] float speedMax = 10, tempsDeplacement, tempsDarret,tempsSaut,hauteurMax,largeurMax,rotationInitial; // vitesse max elle est divisé par 2 sur le parcours normal, le tps pour arriver a destination
    [SerializeField] GameObject Detecteur;
    [SerializeField] bool avecPlatform; // true si parcour contenant une plateform 
    private float speed = 0,horizontal,vertical,posXMax,posXMin;
    private bool enSaut;
    Vector3 pos;
    Vector3 pos2;
    Vector3 taunter;
    private Rigidbody2D m_rigidbody2D;

    void Start ()
    {
        horizontal = transform.position.x;
        vertical = transform.position.y;
        m_rigidbody2D = GetComponent<Rigidbody2D>();
        pos = transform.position;
        if (!avecPlatform)
        {
            hauteurMax = 0;
        }
        pos2 = new Vector3(pos.x+largeurMax, pos.y+hauteurMax, pos.z);
        if (pos2.x > pos.x)
        {
            posXMax = pos2.x;
            posXMin = pos.x;
        }
        else if (pos2.x < pos.x)
        {
            posXMax = pos.x;
            posXMin = pos2.x;
        }
        enSaut = false;
        StartCoroutine("Deplacement");
    }
	
	void Update ()
    {
        transform.position = Vector2.MoveTowards(transform.position,  new Vector2(horizontal, vertical), speed * Time.deltaTime);
    }
    public void Detection(float taunter)
    {
        StartCoroutine(Poursuite(taunter));
    }
    IEnumerator Poursuite(float taunter) // traque le taunter 
    {
        if (enSaut)
        {
            if (transform.position.y != pos.y)
            {
                vertical = pos.y;
                yield return new WaitForSeconds((transform.position.y-pos.y)/speed);
            }
        }

        StopCoroutine("Deplacement");
        speed = speedMax;
        if (transform.position.y != pos.y)
        {
            if (transform.position.x != pos.x || transform.position.x != pos2.x)
            {
                if (taunter > posXMax)
                {
                    if (transform.position.x != posXMax)
                    {
                        horizontal = posXMax;
                        yield return new WaitForSeconds((posXMax-transform.position.x)/speed);
                    }
                }
                else if (taunter < posXMin)
                {
                    if (transform.position.x != posXMin)
                    {
                        horizontal = posXMin;
                        yield return new WaitForSeconds((transform.position.x-posXMin)/speed);
                    }
                }
            }
            vertical = pos.y;
            yield return new WaitForSeconds((transform.position.y - pos.y) / speed);
        }
        if (transform.position.x < taunter)
        {
            transform.rotation = Quaternion.Euler(transform.rotation.x, 180, transform.rotation.z);
            horizontal = taunter;
            yield return new WaitForSeconds((taunter-transform.position.x)/speed);
        }
        else if (transform.position.x > taunter)
        {
            transform.rotation = Quaternion.Euler(transform.rotation.x, 0, transform.rotation.z);
            horizontal = taunter;
            yield return new WaitForSeconds((transform.position.x-taunter) / speed);
        }
        yield return new WaitForSeconds(0.5f);
        StartCoroutine("Rotation");
        yield return new WaitForSeconds(3f);
        StopCoroutine("Rotation");
        yield return new WaitForSeconds(0.5f);
        if (transform.position.x < pos.x)
        {
            transform.rotation = Quaternion.Euler(transform.rotation.x, 180, transform.rotation.z);
            horizontal = pos.x;
            yield return new WaitForSeconds((pos.x-transform.position.x)/speed);
        }
        else if (transform.position.x > pos.x)
        {
            transform.rotation = Quaternion.Euler(transform.rotation.x, 0, transform.rotation.z);
            horizontal = pos.x;
            yield return new WaitForSeconds((transform.position.x-pos.x) / speed);
        }
        yield return new WaitForSeconds(0.1f);
        StartCoroutine("Deplacement");
        yield return new WaitForSeconds(2.0f);
        Detecteur.GetComponent<Detecteur>().Detecter();
    }
    IEnumerator Rotation() // rotation.y quand arrive sur la position ou le taunter a été detecté 
    {
        while (true)
        {
            transform.rotation = Quaternion.Euler(transform.rotation.x, 180, transform.rotation.z);
            yield return new WaitForSeconds(0.6f);
            transform.rotation = Quaternion.Euler(transform.rotation.x, 0, transform.rotation.z);
            yield return new WaitForSeconds(0.6f);
        }
    }

    IEnumerator Deplacement()
    {
        speed = speedMax / 2;
        yield return new WaitForSeconds(1);
        if (avecPlatform)
        {
            while (true)
            {
                transform.rotation = Quaternion.Euler(transform.rotation.x, rotationInitial, transform.rotation.z);
                yield return new WaitForSeconds(0.1f);
                horizontal = pos2.x;
                vertical = transform.position.y;
                yield return new WaitForSeconds(tempsDeplacement + tempsDarret);
                enSaut = true;
                transform.rotation = Quaternion.Euler(transform.rotation.x, rotationInitial + 180, transform.rotation.z);
                yield return new WaitForSeconds(0.1f);
                vertical = pos2.y;
                horizontal = pos2.x;
                yield return new WaitForSeconds(tempsSaut);
                enSaut = false;
                horizontal = pos.x;
                yield return new WaitForSeconds(tempsDeplacement);
                enSaut = true;
                vertical = pos.y;
                yield return new WaitForSeconds(tempsSaut);
                enSaut = false;
                yield return new WaitForSeconds(tempsDarret);
            }
        }
        else
        {
            while (true)
            {
                transform.rotation = Quaternion.Euler(transform.rotation.x, rotationInitial, transform.rotation.z);
                yield return new WaitForSeconds(0.1f);
                horizontal = pos2.x;
                vertical = transform.position.y;
                yield return new WaitForSeconds(tempsDeplacement + tempsDarret);
                transform.rotation = Quaternion.Euler(transform.rotation.x, rotationInitial+180, transform.rotation.z);
                yield return new WaitForSeconds(0.1f);
                horizontal = pos.x;
                yield return new WaitForSeconds(tempsDeplacement + tempsDarret);
            }
        }
    }
    
}
