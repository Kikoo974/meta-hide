﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Archer : MonoBehaviour {
    InventoryScript inventoryScript;
	// Use this for initialization
	void Start () {
        inventoryScript = GameObject.Find("Inventory").GetComponent<InventoryScript>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag=="Player")
        {
            inventoryScript.GameOver();
            GetComponent<ArcherAreaOfView>().StopAllCoroutines();
        }
    }
}
