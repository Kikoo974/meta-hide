﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Detecteur : MonoBehaviour {
    [SerializeField] GameObject chevalierNoir;
    float taunter;
    bool detecter =false;
    void Start () {

    }
	void Update () {
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag=="Taunter" && !detecter) 
        {
            detecter = true;
            taunter = collision.gameObject.transform.position.x;
            chevalierNoir.GetComponent<ChevalierNoir>().Detection(taunter);
        }
    }
    public void Detecter()
    {
        detecter = false;
    }
}
