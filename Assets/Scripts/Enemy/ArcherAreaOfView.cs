﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcherAreaOfView : MonoBehaviour {
	[SerializeField]
	LayerMask groundLayer;
	[SerializeField]
	Material material;
	Vector2[] polygon;
	[SerializeField]int startAngle, endSwift;
	private float angle;
	PolygonCollider2D pgc2D;
	Mesh mesh;
	MeshRenderer meshRenderer;
	MeshFilter filter;
	
	
	// Use this for initialization
	void Start () {
		polygon=new Vector2[20];
		pgc2D=this.gameObject.GetComponent<PolygonCollider2D>();
		angle=Mathf.PI*startAngle/72;
		mesh=new Mesh();
		meshRenderer = gameObject.AddComponent<MeshRenderer>();
		filter = gameObject.AddComponent(typeof(MeshFilter)) as MeshFilter;
		meshRenderer.material= material;
		SetPolygon(angle);
		StartCoroutine(Swifting());
		//StartCoroutine(GoBack());
	}
	
	// Update is called once per frame
	void Update () {
	}
	void SetPolygon(float newAngle){
		angle=newAngle;
		for(int i=-9;i<10;i++){
			float angular= angle+i*Mathf.PI/72f;
			RaycastHit2D rayonMagique;
			rayonMagique=Physics2D.Raycast(this.gameObject.transform.position,this.gameObject.transform.position+new Vector3(1000*Mathf.Cos(angular),1000*Mathf.Sin(angular),0f),14f,groundLayer);
			if(rayonMagique){
				polygon[i+10]=new Vector2((rayonMagique.point.x-this.gameObject.transform.position.x),(rayonMagique.point.y-this.gameObject.transform.position.y));
			}else{
				polygon[i+10]=new Vector2(14*Mathf.Cos(angular),14*Mathf.Sin(angular));
			}
		}
		for(int i=0;i<18;i++){
			Vector2[] array=new Vector2[3]{polygon[0],polygon[i+1],polygon[i+2]};
			pgc2D.SetPath(i,array);
		}
		ResetMesh();
	}
	void RotatePolygon(bool horaire){
		if(horaire){/*angle-=0.05f;
			SetPolygon(); */
			angle-=Mathf.PI/72f;
			for(int i=18;i>0;i--){
				polygon[i+1]=polygon[i];
			}
			float angular = angle-Mathf.PI/8f;
			RaycastHit2D rayonMagique;
			rayonMagique=Physics2D.Raycast(this.gameObject.transform.position,this.gameObject.transform.position+new Vector3(1000*Mathf.Cos(angular),1000*Mathf.Sin(angular),0f),14f,groundLayer);
			if(rayonMagique){
					polygon[1]=new Vector2((rayonMagique.point.x-this.gameObject.transform.position.x),(rayonMagique.point.y-this.gameObject.transform.position.y));
			}else{
					polygon[1]=new Vector2(14*Mathf.Cos(angular),14*Mathf.Sin(angular));
			}
		}else{/* angle+=0.05f;
			SetPolygon();*/
			
			angle+=Mathf.PI/72f;
			for(int i=1;i<19;i++){
				polygon[i]=polygon[i+1];
			}
			float angular = angle+Mathf.PI/8f;
			RaycastHit2D rayonMagique;
			rayonMagique=Physics2D.Raycast(this.gameObject.transform.position,this.gameObject.transform.position+new Vector3(1000*Mathf.Cos(angular),1000*Mathf.Sin(angular),0f),14f,groundLayer);
			if(rayonMagique){
					polygon[19]=new Vector2((rayonMagique.point.x-this.gameObject.transform.position.x)/2,(rayonMagique.point.y-this.gameObject.transform.position.y)/2);
			}else{
					polygon[19]=new Vector2(7*Mathf.Cos(angular),7*Mathf.Sin(angular));
			}
		}
		for(int i=0;i<18;i++){
			Vector2[] array=new Vector2[3]{polygon[0],polygon[i+1],polygon[i+2]};
			pgc2D.SetPath(i,array);
		}
		ResetMesh();
	}
	void ResetMesh(){
		List<Vector3> verticesP=new List<Vector3>{};
		List<int> trianglesP=new List<int>{};
		for(int i=1;i<19;i++){
			verticesP.Add(new Vector3(polygon[i].x,polygon[i].y,-1f));
			verticesP.Add(new Vector3(polygon[0].x,polygon[0].y,-1f));
			verticesP.Add(new Vector3(polygon[i+1].x,polygon[i+1].y,-1f));
			trianglesP.Add(3*i-3);
			trianglesP.Add(3*i-2);
			trianglesP.Add(3*i-1);
		}
		for(int i=1;i<19;i++){
			trianglesP.Add(3*i-3);
			trianglesP.Add(3*i-2);
			trianglesP.Add(3*i-1);
		}
        mesh.SetVertices(verticesP);
        mesh.SetTriangles(trianglesP,0);
        filter.mesh = mesh;
		meshRenderer.material= material;
		filter.sharedMesh.bounds=new Bounds(this.gameObject.transform.position,new Vector3(500.0f,500.0f,500.0f));
	}
	IEnumerator Swifting(){
		int i=0;
		int j=0;
		bool pause=false;
		bool horaire=true;
		while(true){
			if(horaire&&!pause){
				//RotatePolygon(horaire);
				SetPolygon(angle-0.05f);
				i++;
				if(i>endSwift){
					horaire=false;
					pause=true;
					j=0;
				}
			}else if(!pause){
				//RotatePolygon(horaire);
				SetPolygon(angle+0.05f);
				i--;
				if(i<=0){
					horaire=true;
					pause =true;
					j=0;
				}
			}else{
				SetPolygon(angle);
				j++;
				if(j>15){
					pause=false;
				}
			}
			yield return new WaitForSeconds(0.1f);
		}
	}
	IEnumerator GoBack(){
		bool right =true;
		while(true){
			if(right){
				angle=Mathf.PI-angle;
				SetPolygon(angle);
				yield return new WaitForSeconds(3f);
			}else{
				angle=Mathf.PI+angle;
				SetPolygon(angle);
				yield return new WaitForSeconds(3f);
			}
		}
	}

}
