﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arbalete : MonoBehaviour {
    [SerializeField] GameObject arrow;
    [SerializeField] Transform pos;
    [SerializeField] float time = 2f;
    public int shieldNumber;
	// Use this for initialization
	void Start () {
        StartCoroutine(Shoot());
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    IEnumerator Shoot()
    {
        while(true)
        {
            Instantiate(arrow, pos.position, Quaternion.Euler(0, 0, -102));
            yield return new WaitForSeconds(time);
        }
    }
}
