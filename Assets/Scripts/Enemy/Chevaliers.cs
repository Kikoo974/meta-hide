﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Chevaliers : MonoBehaviour {
    [SerializeField]  float speedMax=3,tempsDeplacement,tempsDarret;
    int rotationInitial;
    private float speed = 0;
    bool aller;
    private GameObject player;
    private Rigidbody2D m_rigidbody2D;
    InventoryScript inventoryScript;
    Animator anim;

    void Start () {
        m_rigidbody2D = GetComponent<Rigidbody2D>();
        inventoryScript = GameObject.Find("Inventory").GetComponent<InventoryScript>();
        player = GameObject.FindGameObjectWithTag("Player");
        anim = GetComponent<Animator>();
        if(speedMax < 0)
        {
           rotationInitial = 0;
        }
        else if (speedMax > 0)
        {
            rotationInitial = 180;
        }
        StartCoroutine("Deplacement");
    }
	void Update () {
        
    }
 
    IEnumerator Deplacement()
    {
        yield return new WaitForSeconds(1);
        while (true)
        {
            anim.SetBool("walk", true);
            m_rigidbody2D.velocity = new Vector2(speedMax, 0);
            transform.rotation = Quaternion.Euler(transform.rotation.x, rotationInitial, transform.rotation.z);
            yield return new WaitForSeconds(tempsDeplacement);
            anim.SetBool("walk", false);
            m_rigidbody2D.velocity = new Vector2(0, 0);
            yield return new WaitForSeconds(tempsDarret);
            anim.SetBool("walk", true);
            m_rigidbody2D.velocity = new Vector2(-speedMax, 0);
            transform.rotation = Quaternion.Euler(transform.rotation.x, rotationInitial+180, transform.rotation.z);
            yield return new WaitForSeconds(tempsDeplacement);
            anim.SetBool("walk", false);
            m_rigidbody2D.velocity = new Vector2(0, 0);
            yield return new WaitForSeconds(tempsDarret);
        }
    }
   
}
