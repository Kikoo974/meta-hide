﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Phase2BossTrigger : MonoBehaviour {

    [SerializeField] GameObject Boss, Exclamation;
    private GameObject player;
    bool actived=false;
    void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
    }
	
	
	void Update () {
		
	}
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player"&& !actived)
        {
            actived = true;
            StartCoroutine("StartBossPhase2");
        }
    }
    IEnumerator StartBossPhase2()
    {
        Boss.transform.position = new Vector3(54, 3.4f, 0);
        Boss.transform.rotation = Quaternion.Euler(0, 0, 0);
        Boss.GetComponent<Rigidbody2D>().velocity = new Vector2(-2.5f, 0);
        player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        player.GetComponent<PlayerController>().canMove = false;
        player.GetComponent<PlayerController>().canJump = false;
        yield return new WaitForSeconds(1.0f);
        player.transform.rotation = Quaternion.Euler(0, 180, 0);
        yield return new WaitForSeconds(1.0f);
        var exclamation = Instantiate(Exclamation, new Vector3(player.transform.position.x, player.transform.position.y + 1, transform.position.z), Quaternion.identity);
        yield return new WaitForSeconds(0.5f);
        Destroy(exclamation);
        player.GetComponent<PlayerController>().canMove = true;
        player.GetComponent<PlayerController>().canJump = true;
        Destroy(gameObject);

    }
}
