﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransitionPhase : MonoBehaviour {

    private float speed;
    private Transform player,Boss1;
    private Rigidbody2D p_rigidbody;
    private bool CanTransform;
    private CameraWingBoss CameraBoss;

    private void Start ()
    {
        CanTransform=true;
        speed = 4f;
        player = GameObject.Find("player").GetComponent<Transform>();
        Boss1 = GameObject.Find("Boss").GetComponent<Transform>();
        p_rigidbody = GameObject.Find("player").GetComponent<Rigidbody2D>();
        CameraBoss = GameObject.Find("Main Camera").GetComponent<CameraWingBoss>();

            
        if (Boss1.rotation.y == 0)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(-speed, 0);
        }
        else
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(speed, 0);
        }
        StartCoroutine("Destroy");
    }
	
	// Update is called once per frame
	private void Update () 
    {
    }

     private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Begin")
        {
            p_rigidbody.bodyType = RigidbodyType2D.Dynamic;
            player.transform.parent = null;
            player.GetComponent<PlayerController>().canMove = true;
            CanTransform=false;
        }
        //ramene le player au debut de la platform
         if (other.gameObject.tag == "Player"&& CanTransform)
        {
            CameraBoss.GetComponent<CameraWingBoss>().Follow = false;
            p_rigidbody.bodyType = RigidbodyType2D.Kinematic;
            player.transform.parent = transform;
            player.transform.position = new Vector3(transform.position.x+0.5f, transform.position.y-1f);
            player.GetComponent<PlayerController>().canMove = false;
        }
    }
     IEnumerator Destroy()
    {
        yield return new WaitForSeconds(13f);
        Destroy(gameObject);
    }
}