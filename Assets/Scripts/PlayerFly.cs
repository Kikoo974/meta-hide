﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFly : MonoBehaviour {

    private float fly;
    public GameObject Power;
    GameObject player;
    Rigidbody2D m_rigidbody2D;
    [SerializeField] int enclumeNumber;
    [SerializeField] GameObject[] Etage;
    [SerializeField] GameObject Wind, option;
    private int i;
    private PlayerPref playerPrefsX;
    private CameraWingBoss CameraBoss;
    private void Start ()
    {
        i = 0;
        playerPrefsX = GameObject.Find("PlayerPrefs").GetComponent<PlayerPref>();
        CameraBoss = GameObject.Find("Main Camera").GetComponent<CameraWingBoss>();
        player = GameObject.FindGameObjectWithTag("Player");
        m_rigidbody2D = player.GetComponent<Rigidbody2D>();
        fly = 1000;
	}

    private void Update (){
        if (player.GetComponent<SpriteRenderer>().sprite == GameObject.Find("Inventory").GetComponent<InventoryScript>().transformationSprites[enclumeNumber])
            m_rigidbody2D.mass = 20;
        else
            m_rigidbody2D.mass = 1;
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Tornade")
        {
            CameraBoss.GetComponent<CameraWingBoss>().Follow = false;
            gameObject.GetComponent<Rigidbody2D>().AddForce(Vector2.up * fly);
        }
        if (other.gameObject.tag == "Power")
        {
            Destroy(Power);
            player.GetComponent<PlayerController>().windpower = true;
            playerPrefsX.SetBool("windUnlocked",true);
            Wind.SetActive(true);
            option.SetActive(true);
            Time.timeScale = 0;
        }
        if (other.gameObject.tag =="Etage")
        {
            Etage[i].SetActive(true);
            i++;
            Destroy(other.gameObject);
        }

    }

    private void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "sol")
        {
            CameraBoss.GetComponent<CameraWingBoss>().Follow = true;
        }
    }
}
