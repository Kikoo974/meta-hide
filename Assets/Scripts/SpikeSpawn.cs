﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeSpawn : MonoBehaviour {

private bool Trigger;
	void Start () {
		
	}
	
	
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if ( other.gameObject.tag == "Player")
		{
			Trigger = true;
			StartCoroutine("SpikeUp");
		}
	}
	void OnTriggerExit2D(Collider2D other)
	{
		if ( other.gameObject.tag == "Player")
		{
			Trigger = false;
			StartCoroutine("SpikeUp");
		}
	}

	IEnumerator SpikeUp()
	{
		if (Trigger)
		{
			for( int i=0;i<5;i++)
			{
				transform.Translate(Vector2.up * 0.15f);
				yield return new WaitForSeconds(0f);
			}
			StopCoroutine("SpikeUp");
		}
		else{
			for( int i=0;i<5;i++)
			{
				transform.Translate(Vector2.down * 0.15f);
				yield return new WaitForSeconds(0f);
			}
			StopCoroutine("SpikeUp");
		}
		
	}
}
