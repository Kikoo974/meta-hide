﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoupSurlaTete : MonoBehaviour {
    [SerializeField] GameObject Detect;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnCollisionEnter2D(Collision2D coll)
    {
        if(coll.gameObject.tag == "ObjectFall")
        {
            Detect.SetActive(false);
            gameObject.GetComponent<Collider2D>().enabled = false;
            gameObject.transform.rotation = Quaternion.Euler(0, 0, 90);
        }
    }
}
