﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangementOffSet : MonoBehaviour {
    [SerializeField] float newYPosCam;
    CameraFollowerStory cameraFollowerS;
    // Use this for initialization
    void Start () {
        cameraFollowerS = GameObject.Find("Main Camera").GetComponent<CameraFollowerStory>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            cameraFollowerS.SetYOffSet(newYPosCam);
        }
    }
}
