﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextLevel : MonoBehaviour {

	 private PlayerPref playerPrefsX;
     private PlayerController player;
     private int i,lvlUnlocked,Level,Clear;
    [SerializeField] GameObject Z;
	void Start () {
		playerPrefsX = GameObject.Find("PlayerPrefs").GetComponent<PlayerPref>();
        i = PlayerPrefs.GetInt("Level");
        lvlUnlocked=PlayerPrefs.GetInt("LvlUnlocked");
        Level = SceneManager.GetActiveScene().buildIndex;//niveau actif
        Clear = PlayerPrefs.GetInt("LvlClear");//niveau finis
	}
	
	void Update () {
	}
   void OnTriggerEnter2D (Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            Z.SetActive(true);
            if (Level>Clear)//empêche de finir toujour le même niveau et que les LvlUnlocked n'augmente
            {
            PlayerPrefs.SetInt("Level",i+1);
            PlayerPrefs.SetInt("LvlUnlocked",lvlUnlocked+1);
            }
            PlayerPrefs.SetInt("LvlClear",Level);
        }
    }
    void OnTriggerStay2D (Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (Input.GetKey("z"))
            {
                GameObject.Find("GameManager").GetComponent<GameManager>().levelFinished = false;
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
                GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().StopAllCoroutines();
            }
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
            Z.SetActive(false);
    }
}
