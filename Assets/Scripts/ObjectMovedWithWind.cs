﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMovedWithWind : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	private void OnTriggerEnter2D(Collider2D other) {
		if(other.gameObject.CompareTag("Power")){
			this.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector3(other.gameObject.GetComponent<Rigidbody2D>().velocity.x*100,0f,0f));
			other.GetComponent<WindPower>().DeleteWind();
		}
	}
}
