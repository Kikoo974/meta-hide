﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    [SerializeField] GameObject option,BoutonOtion,boutonPause,Gear;
    [SerializeField] GameObject[] MenuTouche;
    public bool levelFinished;
    private static GameManager current;
    private bool optionActive,ButtonGearActive;
    private int i;
    public GameObject Menu;
    GameObject cage;
    private void Awake()
    {
        if (current == null)
        {
            current = this;
            DontDestroyOnLoad(this);
        }
        else if (current != this)
        {
            Destroy(gameObject);
        }
    }

	private void Start ()
    {
        ButtonGearActive = false;
        Time.timeScale = 1;
        BoutonOtion.SetActive(true);
        boutonPause.SetActive(false);
        option.SetActive(false);
        optionActive = false;
        Menu= GameObject.Find("Menu");
        i = 0;
        
    }

	private void Update ()
    {
        //Activer le menu pause par le bouton ou la touche echap
        if (Input.GetKey(KeyCode.Escape)&&SceneManager.GetActiveScene().name != "Menu" && !optionActive||ButtonGearActive&& !optionActive) 
        {                                                                                                                                       
            ButtonGearActive = false;
            boutonPause.SetActive(true);
            BoutonOtion.SetActive(true);
            Time.timeScale = 0;
        }
        //Active le bouton option dans la scene menu si il trouve l'object "menu" sinon il desactive le bouton
        if(SceneManager.GetActiveScene().name == "Menu"&& GameObject.Find("Menu")&&!BoutonOtion.activeSelf)
        {
            BoutonOtion.SetActive(true);
        }
        else if (SceneManager.GetActiveScene().name == "Menu"&& !GameObject.Find("Menu")&&BoutonOtion.activeSelf)
        {
            BoutonOtion.SetActive(false);
        }
        else{
            //rien faire
        }


        if(SceneManager.GetActiveScene().name != "Menu"&& !Gear.activeSelf)
        {
            Gear.SetActive(true);
        }
        else if (SceneManager.GetActiveScene().name == "Menu"&& Gear.activeSelf)
        {
            Gear.SetActive(false);
        }
        else{
            //rien faire
        }
    }

    public void MainMenu(int nmbr)
    {
            if (SceneManager.GetActiveScene().name == "Menu")
            {
               switch(nmbr)
                {
                case 0:
                    Menu= GameObject.Find("Menu");
                    Menu.SetActive(false);
                    option.SetActive(true);
                    BoutonOtion.SetActive(false);
                    break;
                case 1:
                    option.SetActive(false);
                    Menu.SetActive(true);
                    BoutonOtion.SetActive(true);
                    break;
                default:
                    break;
                }
            }
    }

    public void MenuPause(int chosen)
    {
        if (SceneManager.GetActiveScene().name != "Menu")
        {
            switch(chosen)
            {
                case 0:
                    boutonPause.SetActive(false);
                    BoutonOtion.SetActive(false);
                    Time.timeScale = 1;
                    break;
                case 1:
                    SceneManager.LoadScene("Menu");
                    boutonPause.SetActive(false);
                    BoutonOtion.SetActive(false);
                    Time.timeScale = 1;
                    break;
                case 2:
                    option.SetActive(true);
                    boutonPause.SetActive(false);
                    BoutonOtion.SetActive(false);
                    optionActive = true;
                    break;
                case 3:
                    option.SetActive(false);
                    boutonPause.SetActive(true);
                    BoutonOtion.SetActive(true);
                    optionActive = false;
                    break;
                default:
                break;
            }
        }
    }

    public void MenuOption(int test)
    {
        switch(test)
        {
            case 1:
                MenuTouche[i].SetActive(false);
                i++;
                MenuTouche[i].SetActive(true);
                break;
            case 2:
                MenuTouche[i].SetActive(false);
                i--;
                MenuTouche[i].SetActive(true);
                break;
            default:
                break;
        }       
    }
    public void ResetScene()
    {
        levelFinished = true;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void GearPause()
    {
        ButtonGearActive = true;
    }
}
