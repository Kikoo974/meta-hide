﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenTheGate : MonoBehaviour {
	[SerializeField]GameObject gargoyle1,gargoyle1Pos,gargoyle2,gargoyle2Pos,endDoor;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	private void OnTriggerEnter2D(Collider2D other) {
		if(other.gameObject==gargoyle1){
			gargoyle1Pos.GetComponent<BoxCollider2D>().enabled=false;
			gargoyle1.GetComponent<Rigidbody2D>().isKinematic=true;
			gargoyle1.GetComponent<Rigidbody2D>().velocity=new Vector3(0f,0f,0f);
			gargoyle1.GetComponent<BoxCollider2D>().enabled=false;
			gargoyle1.transform.position=gargoyle1Pos.transform.position;
		}else if(other.gameObject==gargoyle2){
			gargoyle2Pos.GetComponent<BoxCollider2D>().enabled=false;
			gargoyle2.GetComponent<Rigidbody2D>().isKinematic=true;
			gargoyle2.GetComponent<Rigidbody2D>().velocity=new Vector3(0f,0f,0f);
			gargoyle2.GetComponent<BoxCollider2D>().enabled=false;
			gargoyle2.transform.position=gargoyle2Pos.transform.position;
			OpenTheDamnedGate();
		}
	}
	IEnumerator OpenTheDamnedGate(){
		while(this.transform.position.y<=10.5f){
			this.transform.position= Vector2.MoveTowards(this.transform.position,
				new Vector2(this.transform.position.x,10.6f),
				Time.deltaTime);
			yield return new WaitForFixedUpdate();
		}
		endDoor.GetComponent<BoxCollider2D>().enabled=true;
	}
}
