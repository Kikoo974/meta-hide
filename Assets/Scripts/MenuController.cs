﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController : MonoBehaviour {

    [SerializeField] GameObject Menu, option,ChoiseLevel, Yeux, Titre;
    [SerializeField] Button [] lvlButton;
    private string levelName;
    private int save=0,lvlUnlocked,ClearLvl;
    private PlayerPref playerPrefsX;
    Animator doorAnim;
	void Start ()
    {
        Menu.SetActive(true);
        doorAnim = GameObject.Find("Door").GetComponent<Animator>();
        playerPrefsX = GameObject.Find("PlayerPrefs").GetComponent<PlayerPref>();
        save = PlayerPrefs.GetInt("Level");
        lvlUnlocked=PlayerPrefs.GetInt("LvlUnlocked");
        ClearLvl = PlayerPrefs.GetInt("LvlClear");
        SetLevelChoice();
    }

    void Update (){
        
    }
    
    public void MenuChoice(int chosen)
    {
        switch (chosen)
        {
             case 0:

                levelName = "Intro";
                PlayerPrefs.SetInt("Level", 1);
                PlayerPrefs.SetInt("LvlUnlocked",0);
                PlayerPrefs.SetInt("LvlClear",-1);
                playerPrefsX.SetBool("windUnlocked",false);
                StartCoroutine(ChangeLevel());
                break;
             case 1:
                Menu.SetActive(false);
                ChoiseLevel.SetActive(true);
                break;
            case 2 :
                ChoiseLevel.SetActive(false);
                Menu.SetActive(true);
                break;
            case 3:
                Continue();
                break;
            case 4:
                Application.Quit();
                break;
            default:
                break;
        }
    }

    public void ChoisenLevel(int Level)
    {    
        switch (Level)
        {           
            case 0:
                levelName = "Level1-1";
                break;
            case 1:
                levelName = "Level1-2";
                break;
            case 2:
                 levelName = "Level1-3";
                 break;
            case 3:
                 levelName = "Boss1";
                 break;
             case 4:
                levelName = "Level2-1";
                 break;
            case 5:
                levelName = "Level2-2";
                break;
            case 6:
                levelName = "Boss";
                break;
            case 7:
                levelName = "Level3-1";
                break;
            case 8:
                levelName= "Level3-2";
                break;
            default:
                 break;
        }
        StartCoroutine(ChangeLevel());
    }
    private void Continue()
    {
        switch(SceneManager.GetActiveScene().buildIndex + save)
        {
            case 1:
                levelName = "Intro";
                break;
            case 2:
                levelName = "Level1-1";
                break;
            case 3:
                levelName = "Level1-2";
                break;
            case 4:
                levelName = "Level1-3";
                break;
            case 5:
                levelName = "Boss1";
                break;
            case 6:
                levelName = "Level2-1";
                break;
            case 7:
                levelName = "Level2-2";
                break;
            case 8:
                levelName = "Boss";
                break;
            case 9:
                levelName = "Level3-1";
                break;
            case 10:
                levelName = "Level3-2";
                break;
            default:
                break;
        }
        StartCoroutine(ChangeLevel());
    }
    private void SetLevelChoice(){
        for(int i =lvlUnlocked;i<10;i++){
            lvlButton[i].interactable=false;
        }
    }
    IEnumerator ChangeLevel()
    {
        Menu.SetActive(false);
        doorAnim.SetTrigger("ouvert");
        yield return new WaitForSeconds(0.6f);
        Titre.SetActive(false);
        Yeux.SetActive(true);
        yield return new WaitForSeconds(0.4f);
        SceneManager.LoadScene(levelName);
    }
}
