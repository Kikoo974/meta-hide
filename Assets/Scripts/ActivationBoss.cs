﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivationBoss : MonoBehaviour {

    [SerializeField] GameObject Boss,Exclamation;
    private GameObject player;
    bool actived = false;
    CameraFollowerStory CameraFollow;

    void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
        CameraFollow=GameObject.Find("Main Camera").GetComponent<CameraFollowerStory>();
    }
	
	
	void Update () {
		
	}
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player" && !actived)
        {
            actived = true;
            StartCoroutine("StartBoss");
        }
    }
    IEnumerator StartBoss()
    {
        yield return new WaitForSeconds(0.1f);
        Boss.SetActive(true);
        player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        player.GetComponent<PlayerController>().canMove = false;
        player.GetComponent<PlayerController>().canJump = false;
        yield return new WaitForSeconds(1.0f);
        player.transform.rotation = Quaternion.Euler(0, 0, 0);
        yield return new WaitForSeconds(1.0f);
        var exclamation = Instantiate(Exclamation, new Vector3(player.transform.position.x, player.transform.position.y+1, transform.position.z), Quaternion.identity);
        yield return new WaitForSeconds(0.5f);
        Destroy(exclamation);
        player.GetComponent<PlayerController>().canMove = true;
        player.GetComponent<PlayerController>().canJump = true;
        Destroy(gameObject);
    }
}
