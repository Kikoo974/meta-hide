﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerFin : MonoBehaviour {
    [SerializeField] float speed = 2, jumpForce=600f;
    [SerializeField] Transform posBasse, posHaute, posDH, posDB, posHC;
    [SerializeField] Sprite spring, plume;
    [SerializeField] GameObject flame, tornade, Fcamera;
    Sprite basic;
    Rigidbody2D rigid;
    
    // Use this for initialization
    void Start () {
        basic = GetComponent<SpriteRenderer>().sprite;
        rigid = GetComponent<Rigidbody2D>();
        StartCoroutine(Move());
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.name == "Block")
        {
            StartCoroutine(Move2());
        }
    }
    IEnumerator Move()
    {
        while(gameObject.transform.position.x < posBasse.position.x)               // Le Player se déplace jusqu'à la position
        {
            gameObject.transform.position = new Vector2(transform.position.x + Time.deltaTime * speed, transform.position.y);
            yield return new WaitForEndOfFrame();
        }
        flame.SetActive(false);
        GetComponent<SpriteRenderer>().sprite = spring;                            // Le Player se transforme en ressort
        GetComponent<CapsuleCollider2D>().offset = new Vector2(0, 0.3f);
        while (gameObject.transform.localScale.y > 0.3f)
        {
            gameObject.transform.localScale = new Vector2(transform.localScale.x, transform.localScale.y - 0.01f);
            yield return new WaitForEndOfFrame();
        }       
        rigid.AddForce(Vector2.up * jumpForce);
        rigid.AddForce(Vector2.right * jumpForce/2.7f);
        while (gameObject.transform.localScale.y < 1f)
        {
            gameObject.transform.localScale = new Vector2(transform.localScale.x, transform.localScale.y + 0.01f);
            yield return new WaitForEndOfFrame();
        }
        GetComponent<SpriteRenderer>().sprite = basic;
        GetComponent<CapsuleCollider2D>().offset = new Vector2(0, 0);
        
    }
    IEnumerator Move2()
    {
        rigid.velocity = new Vector2(0, 0);
        yield return new WaitForSeconds(0.2f);
        GetComponent<SpriteRenderer>().sprite = plume;
        rigid.isKinematic = true;
        yield return new WaitForSeconds(0.2f);
        tornade.SetActive(true);
        rigid.isKinematic = true;
        while (gameObject.transform.position.x < posDH.position.x)               // Le Player se déplace jusqu'à la position
        {
            gameObject.transform.position = new Vector2(transform.position.x + Time.deltaTime * 1.5f, transform.position.y);
            yield return new WaitForEndOfFrame();
        }
        GetComponent<SpriteRenderer>().sprite = basic;
        tornade.SetActive(false);
        rigid.isKinematic = false;
        while (gameObject.transform.position.x < posDB.position.x)               // Le Player se déplace jusqu'à la position
        {
            gameObject.transform.position = new Vector2(transform.position.x + Time.deltaTime * speed, transform.position.y);
            yield return new WaitForEndOfFrame();
        }
        Fcamera.GetComponent<CameraFollowerStory>().enabled = false;
        yield return new WaitForSeconds(0.2f);
        while (Fcamera.transform.position.y < posHC.position.y)               // Le Player se déplace jusqu'à la position
        {
            Fcamera.transform.position = new Vector3(Fcamera.transform.position.x, Fcamera.transform.position.y + Time.deltaTime, -10);
            yield return new WaitForEndOfFrame();
        }
        yield return new WaitForSeconds(3f);
        SceneManager.LoadScene("Menu");
    }
    
}
