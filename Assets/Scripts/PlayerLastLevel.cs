﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLastLevel : MonoBehaviour
{
    GameObject player;
    Rigidbody2D m_rigidbody2D;
    [SerializeField]int enclumeNumber;
    private float fly;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        m_rigidbody2D = player.GetComponent<Rigidbody2D>();
        fly = 1000;
    }

    private void Update()
    {
        if (player.GetComponent<SpriteRenderer>().sprite == GameObject.Find("Inventory").GetComponent<InventoryScript>().transformationSprites[enclumeNumber])
            m_rigidbody2D.mass = 20;
        else
            m_rigidbody2D.mass = 1;
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Tornade")
        {
            gameObject.GetComponent<Rigidbody2D>().AddForce(Vector2.up * fly);
        }
    }
}
