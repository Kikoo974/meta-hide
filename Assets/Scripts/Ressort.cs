﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ressort : MonoBehaviour {

	// Use this for initialization
    private bool jump;
    private PlayerController player;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (jump)
        {
            player.gameObject.GetComponent<PlayerController>().canJump = true;
            GetComponent<SpriteRenderer>().sprite = null;
        }
		
	}
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "Player")
        {
            jump=true;
           player=other.gameObject.GetComponent<PlayerController>();
        }
    }
}
