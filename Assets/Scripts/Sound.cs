﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class Sound : MonoBehaviour {

	 public AudioMixer mixer;
    public void SetLevel(float sliderValue)
    {
        mixer.SetFloat("VoluParam", Mathf.Log10(sliderValue)*20);
    }

    public void SetFx(float sliderValue)
    {
        mixer.SetFloat("FxParam", Mathf.Log10(sliderValue)*20);
    }
}
