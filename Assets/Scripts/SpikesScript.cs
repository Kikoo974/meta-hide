﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikesScript : MonoBehaviour {
	[SerializeField] GameObject respawnSpot;
	InventoryScript inventory;
	// Use this for initialization
	void Start () {
		inventory=GameObject.Find("Inventory").GetComponent<InventoryScript>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	private void OnCollisionEnter2D(Collision2D other) {
		if(other.gameObject.CompareTag("Player")){
			other.gameObject.transform.position=respawnSpot.transform.position;
		}
	}
}
