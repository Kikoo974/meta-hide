﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Finboss : MonoBehaviour {

    [SerializeField] GameObject Boss;
    bool actived = false;
    void Start () {
		
	}
	
	
	void Update () {
		
	}
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "BossRunner" && !actived)
        {
            actived = true;
            StartCoroutine("FinBoss");
        }
    }
    IEnumerator FinBoss()
    {
        yield return new WaitForSeconds(1.5f);
        Boss.GetComponent<Rigidbody2D>().velocity = new Vector2(0, -3);
        Destroy(gameObject);
    }
}
