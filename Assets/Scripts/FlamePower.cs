﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlamePower : MonoBehaviour {
    private bool power;
    [SerializeField] GameObject flame;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown("r"))
        {
            power = !power;
            flame.SetActive(power);
        }
	}
}
