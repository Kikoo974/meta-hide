﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class InventoryScript : MonoBehaviour {
	[SerializeField] GameObject[] BackGroundMenu;
	[SerializeField] GameObject[] ImagesMenu;
    [SerializeField] GameObject[] FeedbacksMenu;
    /*[SerializeField]*/public Sprite[] transformationSprites;
    [SerializeField] Sprite defaultSprite;
	[SerializeField] float[] scalesOfTransformationX;
	[SerializeField] float[] scalesOfTransformationY;
    [SerializeField] float[] scalesOfTransformationSizeX;
    [SerializeField] float[] scalesOfTransformationSizeY;
	[SerializeField] int numberOfTransfoInThisLevel, numberOfFeedbacks=10;
    int[] numberOfTransformation;

    //Pour l'éternuement
    [SerializeField] GameObject Taunter, FillBar, cage, PV3, PV2, PV1;
    private bool tauntactive = true;   
    private bool timerOn = true;
    private bool feedbacks = true;
    private bool couroutine = true;
    PlayerController pc;
    GameObject player;
    private float timer = 0.0f;
    Image barre;

    int currentTransfo;
    public int pvPlayer;

    // Use this for initialization
    void Start () {
		numberOfTransformation=new int[numberOfTransfoInThisLevel];
		InitializeTransfoMenu();
        currentTransfo=-1;
        pc = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        player = GameObject.FindGameObjectWithTag("Player");
        barre=FillBar.GetComponent<Image>();
        barre.fillAmount=0f;
        StartCoroutine(RoutineTaunter());
        pvPlayer=3;
        GetPv(pvPlayer);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("t") && player.activeInHierarchy)
        {
            timerOn = false;
            timer = 15.0f;
        }
        /* if (Input.GetKeyDown("t") && tauntactive)
        {
            StartCoroutine("Taunt");
        }
        if (timerOn)
        {
            timer += Time.deltaTime;

            if (timer >= 15)
                StartCoroutine("Taunt");
        }*/
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            feedbacks = !feedbacks;
            for(int i=0; i<numberOfFeedbacks;i++)
            {
                FeedbacksMenu[i].SetActive(feedbacks);
            }
        }
        //Pollen.GetComponent<Scrollbar>().size = (timer / 15f);
        //barre.fillAmount = timer / 15;
    }
        
	public void AddTransformation(int itemNumber,int transformationAvailable){
			ImagesMenu[transformationAvailable].SetActive(true);
			ImagesMenu[transformationAvailable].GetComponent<Image>().sprite=transformationSprites[itemNumber];
	}
	private void InitializeTransfoMenu(){
		for(int i=numberOfTransfoInThisLevel;i<10;i++){
			BackGroundMenu[i].SetActive(false);
		}
	}
	public void TransformPlayer(GameObject player,int itemNumber){
        currentTransfo=itemNumber;
        player.GetComponent<PlayerController>().canJump = false;
        player.GetComponent<Animator>().enabled = false;
		player.GetComponent<SpriteRenderer>().sprite=transformationSprites[itemNumber];
		player.GetComponent<Transform>().localScale=new Vector3(scalesOfTransformationX[itemNumber],scalesOfTransformationY[itemNumber],1f);
		player.GetComponent<CapsuleCollider2D>().size=new Vector2(scalesOfTransformationSizeX[itemNumber], scalesOfTransformationSizeY[itemNumber]);
        player.GetComponent<PlayerController>().TestHidden();
	}
    public void GetPv(int pv)
    {
        switch (pv) { 
        
            case 3:
                PV1.SetActive(true);
                PV2.SetActive(true);
                PV3.SetActive(true);
                break;
            case 2:
                PV3.SetActive(false);
                break;
            case 1:
                PV2.SetActive(false);
                break;
            case 0:
                PV1.SetActive(false);
                GameOver();
                break;
            default:
                GameOver();
                break;

        }
      
    }
    public int GetCurrentTransfo(){
        return currentTransfo;
    }
    public void SetCurrentTransfo(){
        currentTransfo=-1;
    }
    public void GameOver()
    {
        couroutine = false;
        cage.SetActive(true);
        pc.canMove = false;
        Taunter.SetActive(true);
        player.GetComponent<CapsuleCollider2D>().size = new Vector2(0.6f, 0.6f);
        player.GetComponent<SpriteRenderer>().sprite = defaultSprite;
        player.GetComponent<Transform>().localScale = new Vector3(1f, 1f, 1f);
        player.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        StopCoroutine(RoutineTaunter());
        StartCoroutine("Lose");
    }
    public void DamageTakenByPlayer(){
        pvPlayer-=1;
        GetPv(pvPlayer);
    }
    /* IEnumerator Taunt()
    {
        timerOn = false;
        timer = 15.0f;
        Taunter.SetActive(true);
        tauntactive = false;
        pc.StartMetamorphose();
        yield return new WaitForSeconds(1f);
        player.GetComponent<Animator>().enabled = true;
        tauntactive = true;
        Taunter.SetActive(false);
        timerOn = true;
        timer = 0.0f;

    }*/
    IEnumerator Lose()
    {
        yield return new WaitForSeconds(2f);
        GameObject.Find("GameManager").GetComponent<GameManager>().ResetScene();
    }
    IEnumerator RoutineTaunter(){
        while(couroutine){
            while(timerOn){
                    if(player.activeInHierarchy)      //Stop le timer si le player disaprait
                        timer += Time.deltaTime;
                    if (timer>=15){
                        timerOn=false;
                }
                barre.fillAmount = timer / 15;
                yield return new WaitForFixedUpdate();
            }
            Taunter.SetActive(true);
            if (SceneManager.GetActiveScene().name == "Boss1")
            {
                pc.canJump = false;
            }
            else
            {
                player.transform.parent = null;
                pc.canJump = true;
            }
            pc.Taunt = false;
            pc.canTransform = false;
            player.GetComponent<CapsuleCollider2D>().size = new Vector2(0.6f, 0.6f);
            player.GetComponent<SpriteRenderer>().sprite = defaultSprite;
            player.GetComponent<Transform>().localScale = new Vector3(1f, 1f, 1f);
            SetCurrentTransfo();
            pc.hidden = false;
            yield return new WaitForSeconds(0.2f);
            player.GetComponent<Animator>().enabled = true;
            Taunter.SetActive(false);
            timer = 0.0f;
            timerOn = true;
            pc.canTransform = true;
        }
    }
}
