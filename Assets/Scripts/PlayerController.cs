﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class PlayerController : MonoBehaviour {
	InventoryScript inventoryScript;
    [SerializeField] PhysicsMaterial2D slide;
    [SerializeField] LayerMask groundLayer;
	[SerializeField]float speed;
	float jumpForce, springInitialY;
	[SerializeField]Sprite defaultSprite, ressortSprite;
	[SerializeField]GameObject spring;
    private int[] getTransformation=new int[10];
	private int transformationAvailable, cachetteActuelle,pv,test;
	private bool cachetteTable,cachetteEtagere,cachette,baliste,freeCam;
	public bool hidden,canMove, canJump, launching,canPower,windpower,canTransform,Taunt;
    public GameObject powerwind,Wind;
    Animator anim;
    /* On va référencer les transformation par valeur d'int:
		1:table
		2:étagère
		3:ressort
		4:flèche
		5:banane */
    // Use this for initialization
    void Start () {
		inventoryScript=GameObject.Find("Inventory").GetComponent<InventoryScript>();
        anim = GetComponent<Animator>();
        transformationAvailable =0;
		cachetteActuelle=-1;
		canMove=true;
		canTransform=false;
        windpower=false;
		jumpForce=300f;
		canPower=true;
		freeCam=false;
		springInitialY=spring.transform.localPosition.y;
		//Initialisation des transformation à chaque début de scène
		for(int i=0;i<getTransformation.Length;i++){
			getTransformation[i]=-1;
		}
		
		if(SceneManager.GetActiveScene().name == "Boss1" || SceneManager.GetActiveScene().name == "Intro"){
			canJump=false;
		}
		  if (GameObject.Find("PlayerPrefs").GetComponent<PlayerPref>().GetBool("windUnlocked")){
            windpower = true;
        }
    }
	// Update is called once per frame
	void Update () {
		if(freeCam){
			return;
		}
		if(canJump&&Input.GetButtonDown("Jump")){
			RaycastHit2D hit =Physics2D.Linecast(transform.position, 
			transform.position - (transform.up * 0.5f), 
			groundLayer);
			if(hit){
				canMove=false;
				StartCoroutine(startJump());
			}
		}
		if(canMove){
			float x = Input.GetAxis("Horizontal");
			Move(x);
		}
		if(canTransform){
			if(Input.GetKeyDown("1")&&getTransformation[0]!=-1){
				inventoryScript.TransformPlayer(this.gameObject,getTransformation[0]);
			}else if(Input.GetKeyDown("2")&&getTransformation[1]!=-1){
				inventoryScript.TransformPlayer(this.gameObject,getTransformation[1]);
			}else if(Input.GetKeyDown("3")&&getTransformation[2]!=-1){
				inventoryScript.TransformPlayer(this.gameObject,getTransformation[2]);
			}else if(Input.GetKeyDown("4")&&getTransformation[3]!=-1){
				inventoryScript.TransformPlayer(this.gameObject,getTransformation[3]);
			}else if(Input.GetKeyDown("5")&&getTransformation[4]!=-1){
				inventoryScript.TransformPlayer(this.gameObject,getTransformation[4]);
			}else if(Input.GetKeyDown("6")&&getTransformation[5]!=-1){
				inventoryScript.TransformPlayer(this.gameObject,getTransformation[5]);
			}else if(Input.GetKeyDown("7")&&getTransformation[6]!=-1){
				inventoryScript.TransformPlayer(this.gameObject,getTransformation[6]);
			}else if(Input.GetKeyDown("8")&&getTransformation[7]!=-1){
				inventoryScript.TransformPlayer(this.gameObject,getTransformation[7]);
			}else if(Input.GetKeyDown("9")&&getTransformation[8]!=-1){
				inventoryScript.TransformPlayer(this.gameObject,getTransformation[8]);
			}else if(Input.GetKeyDown("0")&&getTransformation[9]!=-1){
				inventoryScript.TransformPlayer(this.gameObject,getTransformation[9]);
			}           
        }
        if (Input.GetKeyDown("e")&&windpower&&canPower)
        {
			RaycastHit2D Hit =Physics2D.Linecast(transform.position,transform.position - (transform.up * 0.5f), groundLayer);
            if (Hit)
            {
            	 Wind =Instantiate(powerwind, new Vector3(transform.position.x, transform.position.y, 0), Quaternion.Euler(0, 0, 0));
				canPower = false;
            }
        }
		else{
			if (Input.GetKeyDown("e")&& Wind != null)
			{
				WindPower Wind = GameObject.Find("Powertornade(Clone)").GetComponent<WindPower>();
				Wind.DeleteWind();
			}
		}
        
	}
	void Move(float move) // deplacement
    {
        if (canMove)
        {
			
            if (Mathf.Abs(move) > 0)
            {
                Quaternion rot = transform.rotation;
                transform.rotation = Quaternion.Euler(rot.x, Mathf.Sign(move) == 1 ?180:0, rot.z);
            }
            this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(move * speed, this.gameObject.GetComponent<Rigidbody2D>().velocity.y);

        }
    }
    void OnTriggerEnter2D(Collider2D other)
    { 
        if(other.gameObject.CompareTag("Item")){
			if(!IsTransformationAlreadyAvailable(other.GetComponent<ItemScript>().itemNumber)){
				canTransform=true;
				getTransformation[transformationAvailable]=other.GetComponent<ItemScript>().itemNumber;
				inventoryScript.AddTransformation(other.GetComponent<ItemScript>().itemNumber,transformationAvailable);
				transformationAvailable++;
			}
		}
        if (other.gameObject.CompareTag("Cachette")){
			cachetteActuelle=other.GetComponent<CachetteScript>().cachetteNumber;
			TestHidden();
		}
    }
	void OnCollisionEnter2D (Collision2D coll)
    {
        if(coll.gameObject.tag =="Item"){
			if(!IsTransformationAlreadyAvailable(coll.gameObject.GetComponent<ItemScript>().itemNumber)){
				getTransformation[transformationAvailable]=coll.gameObject.GetComponent<ItemScript>().itemNumber;
				inventoryScript.AddTransformation(coll.gameObject.GetComponent<ItemScript>().itemNumber,transformationAvailable);
				transformationAvailable++;
			}
		}
        if (coll.gameObject.tag == "Trap"){
            inventoryScript.DamageTakenByPlayer();
        }

        if ((coll.gameObject.tag == "sol" || coll.gameObject.tag == "platform") && launching){
			canMove = true;
            launching = false;
		}   
    }
	void OnTriggerExit2D(Collider2D other){
        if(other.gameObject.CompareTag("Cachette")){
			cachetteActuelle=-1;
            hidden = false;
		}
    }
	private bool IsTransformationAlreadyAvailable(int i){
		foreach(int j in getTransformation){
			if(i==j){
				return true;
			}
		}
		return false;
	}
	public void TestHidden(){
        if (inventoryScript.GetCurrentTransfo() == cachetteActuelle)
        {
            hidden = true;
        } else{
			hidden=false;
		}
	}
	public void SetFreeCam(bool libre){
		freeCam=libre;
	}

	IEnumerator startJump(){
        //transformation ressort
		canTransform=false;
        anim.enabled = false;
		this.gameObject.GetComponent<Rigidbody2D>().velocity=new Vector3(0f,0f,0f);
        this.gameObject.GetComponent<SpriteRenderer>().enabled=false;
		spring.SetActive(true);
        this.gameObject.GetComponent<CapsuleCollider2D>().sharedMaterial = null;
		while(Input.GetButton("Jump")&&jumpForce<600f){
			jumpForce+=4f;
			float springScale=spring.transform.localScale.x;
            spring.GetComponent<Transform>().localScale=new Vector3(0.99f*springScale,2f,1f);
			spring.GetComponent<Transform>().localPosition=new Vector3(
				spring.transform.localPosition.x,
				spring.transform.localPosition.y-0.0025f*springScale,
				spring.transform.localPosition.z);
			yield return new WaitForFixedUpdate();

		}
		while(Input.GetButton("Jump")){
			yield return new WaitForFixedUpdate();
		}
		this.gameObject.GetComponent<Rigidbody2D>().AddForce(Vector3.up*jumpForce);
		canMove=true;
		while (this.gameObject.GetComponent<Rigidbody2D>().velocity.y>=0){
			if(spring.transform.localScale.x<2f+jumpForce*0.001f){
				float springScale=spring.transform.localScale.x;
				spring.GetComponent<Transform>().localScale=new Vector3(1.1f*springScale,2f,1f);
				spring.GetComponent<Transform>().localPosition=new Vector3(
					spring.transform.localPosition.x,
					spring.transform.localPosition.y+0.0025f*springScale,
					spring.transform.localPosition.z);
			}          
            yield return new WaitForFixedUpdate();
		}
		spring.GetComponent<Transform>().localPosition=new Vector3(
				spring.transform.localPosition.x,
				springInitialY,
				spring.transform.localPosition.z);
		jumpForce=300f;
		spring.GetComponent<Transform>().localScale=new Vector3(2f,2f,1f);
		spring.SetActive(false);
        anim.enabled = true;
        this.gameObject.GetComponent<SpriteRenderer>().enabled=true;
        this.gameObject.GetComponent<CapsuleCollider2D>().sharedMaterial = slide;
		canTransform=true;
        
        
        //StartCoroutine(Metamorphose(0f));
    }
}
