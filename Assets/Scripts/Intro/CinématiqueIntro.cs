﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CinématiqueIntro : MonoBehaviour {
    private bool move;
    public GameObject detruit;
    public GameObject trou;
    public GameObject blackscreen;
    [SerializeField] GameObject player;
    Collider2D labocoll;
    Animator anim;
	// Use this for initialization
	void Start () {
        StartCoroutine("Cinematique");
        anim = GameObject.Find("labo").GetComponent<Animator>();
        labocoll = GameObject.Find("labo").GetComponent<EdgeCollider2D>();

    }
	
	// Update is called once per frame
	void Update () {
        if (move)
        {
            GameObject.Find("Main Camera").transform.position = new Vector3(Mathf.PingPong(Time.time, 0.1f) * 10, -1.69f, -10);
        }
    }
    IEnumerator Cinematique()
    {
        yield return new WaitForSeconds(2f);
        player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        player.GetComponent<PlayerController>().canMove = false;
        player.GetComponent<PlayerController>().canJump = false;
        move = true;
        yield return new WaitForSeconds(2f);
        move = false;
        blackscreen.SetActive(true);
        yield return new WaitForSeconds(1f);
        player.GetComponent<PlayerController>().canMove = true;
        player.GetComponent<PlayerController>().canJump = true;
        blackscreen.SetActive(false);
        detruit.SetActive(false);
        trou.SetActive(true);
        anim.SetTrigger("broken");
        labocoll.enabled = false;
    }
}
