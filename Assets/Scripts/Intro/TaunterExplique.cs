﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaunterExplique : MonoBehaviour {
    [SerializeField] GameObject Taunter, optionTaunt;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Taunter.activeInHierarchy)
        {
            optionTaunt.SetActive(true);
            Time.timeScale = 0;
            Destroy(this.gameObject);
        }
	}
}
