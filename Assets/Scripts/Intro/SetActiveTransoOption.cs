﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetActiveTransoOption : MonoBehaviour {
    [SerializeField] GameObject tranfoOption;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            tranfoOption.SetActive(true);
            Time.timeScale = 0;
            Destroy(this);
        }
    }
}
