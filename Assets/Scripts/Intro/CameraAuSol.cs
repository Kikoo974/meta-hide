﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAuSol : MonoBehaviour {
    private bool camerablock = true;
    [SerializeField] GameObject optionCamera;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            GameObject.Find("Main Camera").GetComponent<CameraFollowerStory>().enabled = true;
            Time.timeScale = 0;
            optionCamera.SetActive(true);
            Destroy(this.gameObject);
        }
    }
}
