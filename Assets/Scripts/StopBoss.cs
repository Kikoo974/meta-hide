﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopBoss : MonoBehaviour {
    [SerializeField]GameObject Boss;


    void Start () {
		
	}
	
	
	void Update () {
		
	}
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (!GameObject.Find("BossRunner").GetComponent<BossRunner>().firstStop)
            { 
            Boss.GetComponent<Rigidbody2D>().velocity = new Vector2(4, 0);
            }
            Destroy(gameObject);
        }
    }
}
