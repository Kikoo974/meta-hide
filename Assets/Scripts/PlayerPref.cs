﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPref : MonoBehaviour {

    private static PlayerPref current;
    private void Awake()
    {
        if (current == null)
        {
            current = this;
            DontDestroyOnLoad(this);
        }
        else if (current != this)
        {
            Destroy(gameObject);
        }
    }

    public void SetBool(string prefsName, bool boolState){
		PlayerPrefs.SetInt(prefsName, boolState ? 1 : 0);
	}
    //Get the value of an int PlayerPrefs transformed to a bool
    public bool GetBool(string prefsName)
    {
        return PlayerPrefs.GetInt(prefsName) == 1 ? true : false;
    }
}
