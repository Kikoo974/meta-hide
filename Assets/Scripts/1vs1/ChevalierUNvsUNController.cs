﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChevalierUNvsUNController : MonoBehaviour {
    public bool canmove = true;
    public int coup = 2;
    public float speed;
    private bool find;
    Rigidbody2D m_rigidbody2D;
    [SerializeField] LayerMask groundLayer;
    [SerializeField] float jumpForce = 300f;

    // Use this for initialization
    void Start () {
        m_rigidbody2D = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
        float x = Input.GetAxis("Horizontal");
        bool jump = Input.GetButtonDown("Jump");
        if (canmove)
        {
            Move(x, jump);

            if (Input.GetKeyDown("f"))
            {
                coup -= 1;
                if (find == true)
                {
                    var levelmanager = GameObject.Find("LevelManager").GetComponent<LevelManagerUNvsUN>();
                    levelmanager.gameclear = true;
                    levelmanager.playerwin = 2;
                }
            }

        }
	}
    void Move(float move, bool jump) // deplacement
    {
        if (GameObject.Find("Main Camera").GetComponent<CameraFollower>().libre == false && canmove)
        {
            if (Mathf.Abs(move) > 0)
            {
                Quaternion rot = transform.rotation;
                transform.rotation = Quaternion.Euler(rot.x, Mathf.Sign(move) == 1 ? 0 : 180, rot.z);
            }

            m_rigidbody2D.velocity = new Vector2(move * speed, m_rigidbody2D.velocity.y);
            RaycastHit2D hit = Physics2D.Linecast(
                transform.position,
                transform.position - (transform.up * 0.7f),
                groundLayer);

            if (jump && hit.collider)
            {
                jump = false;
                this.gameObject.GetComponent<Rigidbody2D>().AddForce(Vector2.up * jumpForce);
            }
        }
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.name == "Metamorph")
            find = true;
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.name == "Metamorph")
            find = false;
    }
}
