﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelManagerUNvsUN : MonoBehaviour {
    private bool player1turn = true;
    private GameObject metamorph;
    private GameObject chevalier;
    public int playerwin;
    public bool gameclear;
    private bool startcoroutine = false;
    Text wintext;
    // Use this for initialization
    void Start () {
        metamorph = GameObject.Find("Metamorph");
        chevalier = GameObject.Find("Chevalier");
        wintext = GameObject.Find("WinText").GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		if(player1turn)
        {
            chevalier.GetComponent<ChevalierUNvsUNController>().canmove = false;
            chevalier.tag = "Untagged";
            
            metamorph.GetComponent<MetamorphController>().canmove = true;
            metamorph.tag = "Player";
            if(startcoroutine==false)
                StartCoroutine("Player1Turn");

        }
        else
        {
            metamorph.tag = "Untagged";
            metamorph.GetComponent<MetamorphController>().canmove = false;
           
            chevalier.tag = "Player";
            chevalier.GetComponent<ChevalierUNvsUNController>().canmove = true;
        }
        if (chevalier.GetComponent<ChevalierUNvsUNController>().coup == 0)
        {
            player1turn = true;
            chevalier.GetComponent<ChevalierUNvsUNController>().coup = 2;
        }
        if(gameclear ==true)
        {
            Time.timeScale = 0;
            if (playerwin == 1)
                wintext.text = "Player1 Win !!!" ;
            else if (playerwin==2)
                wintext.text = "Player2 Win !!!";

        }
	}
    IEnumerator Player1Turn()
    {
        startcoroutine = true;
        yield return new WaitForSeconds(10f);
        player1turn = false;
        startcoroutine = false;
    }
}
