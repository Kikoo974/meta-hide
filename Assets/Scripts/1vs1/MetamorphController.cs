﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MetamorphController : MonoBehaviour {
    public bool canmove = true;
   Sprite basic;
    public float speed;
    public float jumpForce = 300f;
    [SerializeField] LayerMask groundLayer;
    private Rigidbody2D m_rigidbody2D;
    
    // Use this for initialization
    void Start () {
        basic = GetComponent<SpriteRenderer>().sprite;
        m_rigidbody2D = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
        float x = Input.GetAxis("Horizontal");
        bool jump = Input.GetButtonDown("Jump");
        if (canmove)
        {
            gameObject.layer = 0;
            GetComponent<Rigidbody2D>().isKinematic = false;
            Move(x, jump);
            if (Input.GetKeyDown("1"))
                GetComponent<SpriteRenderer>().sprite = GameObject.Find("Objet1").GetComponent<SpriteRenderer>().sprite;
            else if (Input.GetKeyDown("2"))
                GetComponent<SpriteRenderer>().sprite = GameObject.Find("Objet2").GetComponent<SpriteRenderer>().sprite;
            else if (Input.GetKeyDown("3"))
                GetComponent<SpriteRenderer>().sprite = GameObject.Find("Objet3").GetComponent<SpriteRenderer>().sprite;
            else if (Input.GetKeyDown("4"))
                GetComponent<SpriteRenderer>().sprite = GameObject.Find("Objet4").GetComponent<SpriteRenderer>().sprite;
            else if (Input.GetKeyDown("5"))
                GetComponent<SpriteRenderer>().sprite = GameObject.Find("Objet5").GetComponent<SpriteRenderer>().sprite;

        }
        else
        {
            gameObject.layer = 8;
            m_rigidbody2D.isKinematic = true;
            m_rigidbody2D.velocity = new Vector2(0, 0);
        }
	}
    void Move(float move, bool jump) // deplacement
    {
        if (GameObject.Find("Main Camera").GetComponent<CameraFollower>().libre == false && canmove)
        {
            if (Mathf.Abs(move) > 0)
            {
                Quaternion rot = transform.rotation;
                transform.rotation = Quaternion.Euler(rot.x, Mathf.Sign(move) == 1 ? 0 : 180, rot.z);
            }

            m_rigidbody2D.velocity = new Vector2(move * speed, m_rigidbody2D.velocity.y);
            RaycastHit2D hit =
              Physics2D.Linecast(transform.position, transform.position - (transform.up * 0.5f), groundLayer);

            if (jump && hit.collider)
            {
                jump = false;
                this.gameObject.GetComponent<Rigidbody2D>().AddForce(Vector2.up * jumpForce);
            }
        }
    }
}
