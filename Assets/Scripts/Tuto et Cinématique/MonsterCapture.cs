﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterCapture : MonoBehaviour {
    private bool found;
    PlayerController pc;
    [SerializeField] GameObject cage;
	// Use this for initialization
	void Start () {
        pc = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        
        if (GameObject.Find("GameManager").GetComponent<GameManager>().levelFinished)
            Destroy(gameObject);
        else
            pc.canMove = false;
    }
	
	// Update is called once per frame
	void Update () {
        if (!found)
        {
            pc.canMove = false;
            transform.position = new Vector2(transform.position.x - Time.deltaTime, transform.position.y);
        }
        if (Input.GetButtonDown("Jump"))
            Destroy(gameObject);

    }
    void OnTriggerEnter2D (Collider2D other)
    {
        if(other.gameObject.tag == "Archer")
        {
            StartCoroutine("Found");
        }
    }
    IEnumerator Found()
    {
        found = true;
        cage.SetActive(true);
        yield return new WaitForSeconds(2f);
        pc.canMove = true;
        Destroy(gameObject);
    }

}
