﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoutonPressoire : MonoBehaviour {
	[SerializeField] GameObject[] attachedObject;
	[SerializeField] Sprite anvilSprite;
	[SerializeField] int serialNumber;
	GameObject player;
	bool isTriggerActive;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	private void OnTriggerEnter2D(Collider2D other) {
		if(other.gameObject.CompareTag("Player")){
			isTriggerActive=true;
			StartCoroutine(whileTriggered(other.gameObject));
		}
	}
	private void OnTriggerExit2D(Collider2D other) {
		if(other.gameObject.CompareTag("Player")){
			isTriggerActive=false;
		}
	}
	IEnumerator whileTriggered(GameObject player){
		while(isTriggerActive){
			if(player.GetComponent<SpriteRenderer>().sprite==anvilSprite){
				if(serialNumber==0){
					attachedObject[0].SetActive(true);
					this.gameObject.SetActive(false);
					break;
				}else if(serialNumber==1){
					attachedObject[1].SetActive(false);
					attachedObject[0].GetComponent<BoxCollider2D>().enabled=true;
					attachedObject[0].GetComponent<NextLevel>().enabled=true;
					this.gameObject.SetActive(false);
					break;
				}else{
					break;
				}
				
			}
			yield return new WaitForFixedUpdate();
		}
	}
}
