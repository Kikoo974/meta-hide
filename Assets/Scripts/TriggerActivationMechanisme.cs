﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerActivationMechanisme : MonoBehaviour {

    [SerializeField] GameObject TextTrigger;
	void Start () {
		
	}
	
	
	void Update () {
		
	}
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            TextTrigger.SetActive(true);
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            TextTrigger.SetActive(false);
        }
    }
}
