﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoutonDestroy : MonoBehaviour {
    [SerializeField] GameObject MurFin;
    [SerializeField] Sprite anvilSprite;
    private bool isTriggerActive;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            isTriggerActive = true;
            StartCoroutine(whileTriggered(other.gameObject));
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            isTriggerActive = false;
        }
    }
    IEnumerator whileTriggered(GameObject player)
    {
        while (isTriggerActive)
        {
            if (player.GetComponent<SpriteRenderer>().sprite == anvilSprite)
            {
                MurFin.GetComponent<triggerPorteFin>().GetNumber();

                Destroy(this.gameObject);
            }
            yield return new WaitForFixedUpdate();
        }
    }
}
