﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivationFire : MonoBehaviour {
    [SerializeField] GameObject Feu, text , flame, option;
    // Use this for initialization
    void Start () {
       
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "Player")
        {
            other.gameObject.GetComponent<FlamePower>().enabled = true;
            Feu.SetActive(true);
            flame.SetActive(true);
            text.SetActive(false);
            option.SetActive(true);
            Time.timeScale = 0;
            Destroy(this.gameObject);
        }
    }
}
