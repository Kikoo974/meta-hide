﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBoss : MonoBehaviour {
    public int vie=3;
    private bool nontouch = true;
    Rigidbody2D rigid;
    GameObject player;
    [SerializeField] GameObject pos1;
    [SerializeField] GameObject pos2;
    [SerializeField] GameObject pos3;
    [SerializeField] GameObject defeatText;
    [SerializeField] GameObject flamepower;
    [SerializeField] int enclumeNumber;
    Animator anim;

    // Use this for initialization
    void Start () {
        rigid = GetComponent<Rigidbody2D>();
        player = GameObject.FindGameObjectWithTag("Player");
        anim = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () { 
		if(nontouch)
        {
            if (vie == 3)
            {
                transform.position = Vector2.MoveTowards(transform.position, new Vector2(pos1.transform.position.x, pos1.transform.position.y), 2f * Time.deltaTime);
            }
            else if (vie == 2)
            {
                transform.position = Vector2.MoveTowards(transform.position, new Vector2(pos2.transform.position.x, pos2.transform.position.y), 6f * Time.deltaTime);
                if (transform.position.y >= pos2.transform.position.y)
                    transform.rotation = Quaternion.Euler(0, 180, 0);
            }
            else if (vie == 1)
            {

                transform.position = Vector2.MoveTowards(transform.position, new Vector2(pos3.transform.position.x, pos3.transform.position.y), 10f * Time.deltaTime);
                if (transform.position.y >= pos3.transform.position.y)
                    transform.rotation = Quaternion.Euler(0, 0, 0);
            }
            else
            {
                defeatText.SetActive(true);
                flamepower.SetActive(true);
                anim.SetTrigger("die");
                Destroy(this.gameObject, 0.3f);
            }
        }


	}
    IEnumerator Fall()
    {
        anim.SetBool("fire", true);
        nontouch = false;       
        rigid.isKinematic = false;
        yield return new WaitForSeconds(8f);
        rigid.isKinematic = true;
        nontouch = true;
        anim.SetBool("fire", false);

    }
   void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "fireBall")
            StartCoroutine("Fall");

        if (coll.gameObject.tag == "Player" && 
            player.GetComponent<SpriteRenderer>().sprite == GameObject.Find("Inventory").GetComponent<InventoryScript>().transformationSprites[enclumeNumber] && 
            nontouch == false)
        {
            vie -= 1;
            StopAllCoroutines();
            rigid.isKinematic = true;
            nontouch = true;
            anim.SetBool("fire", false);
        }
    }
}
