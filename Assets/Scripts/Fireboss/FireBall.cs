﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBall : MonoBehaviour {
    GameObject player;
    public bool touch = false;
    public float speed;
    private int direction = 0;
    [SerializeField] int mirrorNumber;
    // Use this for initialization
    void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
       

        if(!touch)
            transform.position = Vector2.MoveTowards(transform.position, new Vector2(player.transform.position.x, player.transform.position.y), 2f * Time.deltaTime);
        else
        {
          
            transform.Translate(Vector3.right*speed);
        }
     
        if (transform.position.x > player.transform.position.x)
        {
            transform.rotation = Quaternion.Euler(0, 0f, 0);
        }
        else if (transform.position.x < player.transform.position.x)
        {
            transform.rotation = Quaternion.Euler(0, 180f, -0);
        }

    }
    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            if (player.GetComponent<SpriteRenderer>().sprite == GameObject.Find("Inventory").GetComponent<InventoryScript>().transformationSprites[mirrorNumber])
            {
                gameObject.tag = "fireBall";
                touch = true;
            }
            else
                Destroy(this.gameObject);
        }
        if (coll.gameObject.tag == "Boss" ||coll.gameObject.tag == "sol")
            Destroy(this.gameObject);
    }
}
