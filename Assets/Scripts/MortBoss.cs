﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MortBoss : MonoBehaviour {

    GameManager gameManager;

    void Start () {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }
	

	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            gameManager.ResetScene();
        }
        if(other.gameObject.tag == "Boss")
        {
            Destroy(other.gameObject);
        }
    }
}
