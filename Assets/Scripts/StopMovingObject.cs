﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopMovingObject : MonoBehaviour {

	[SerializeField] GameObject MoveObject, NotMovingObject;
	[SerializeField] float PosX, posX2;
	private PlayerController player;
	private  WindPower wind;
	void Start () 
	{
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnTriggerEnter2D(Collider2D other)
    {
		if (other.gameObject.tag == "Power")
		{
            Destroy(other.gameObject);
            player.canPower = true;
			MoveObject.transform.position=new Vector2(PosX,MoveObject.transform.position.y);
            NotMovingObject.transform.position = new Vector2(posX2, NotMovingObject.transform.position.y);
        } 
	}
}
