﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour {
   
    GameObject objet;
    CameraFollowerStory cameraFollowerS;
    [SerializeField] GameObject teleport;
    [SerializeField] float newYPosCam;
    bool trigger;
	// Use this for initialization
	void Start () {
       cameraFollowerS= GameObject.Find("Main Camera").GetComponent<CameraFollowerStory>();
	}
	
	// Update is called once per frame
	void Update () {

	}
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            trigger=true;
            StartCoroutine(TriggerActif());
        }
    }
    void OnTriggerExit2D(Collider2D other) {
        if(other.gameObject.tag=="Player"){
            trigger=false;
        }
    }
    IEnumerator TriggerActif(){
        while(trigger){
            if(Input.GetKeyDown("z")){
                cameraFollowerS.SetYOffSet(newYPosCam);
                cameraFollowerS.MoveCameraToward(teleport);
                break;
            }
            else{
                yield return new WaitForFixedUpdate();
            }
        }
    }
}
