﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tornade : MonoBehaviour {

    private Transform Boss1;
    private float speed;

    private void Start()
    {
        speed = 1.5f;
        Boss1 = GameObject.Find("Boss").GetComponent<Transform>();
        if (Boss1.rotation.y == 0)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(-speed, 0);
        }
        else
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(speed, 0);
        }
        StartCoroutine("Destroy");
    }

    IEnumerator Destroy()
    {
        yield return new WaitForSeconds(13f);
        Destroy(gameObject);
    }

    public void DestroyTronade()
    {
        Destroy(gameObject);
    }
}
