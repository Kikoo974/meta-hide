﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss1 : MonoBehaviour
{
    [SerializeField] GameObject tornade, spawn,tornade2,Plume,Air1,Air2,Air3,pos2,pos3,EtageA,EtageB,EtageC;
    [SerializeField] int enclumeNumber;
    private GameObject power,player;
    private GameObject[] Torn;
    private int vie;
    private bool changeTornade,immuBoss,Coroutine;
    private Animator anim;

    private void Start()
    {
        Coroutine =true;
        immuBoss = false;
        changeTornade = false;
        vie = 3;
        power = GameObject.Find("Powertornade");
        StartCoroutine("SpawnTornade1");
        power.SetActive(false);
        anim = GameObject.Find("Boss").GetComponent<Animator>();
        player = GameObject.FindGameObjectWithTag("Player");
    }

   private void Update() 
    {
        if (vie == 2 && !changeTornade&&transform.position != pos2.transform.position)
        {
            Plume.SetActive(true);
            transform.position = Vector2.MoveTowards(transform.position, new Vector2(pos2.transform.position.x, pos2.transform.position.y), 6f * Time.deltaTime);
            if (transform.position.y >= pos2.transform.position.y && Coroutine)
            {
                Coroutine=false;
                transform.rotation = Quaternion.Euler(0, 180, 0);
                StartCoroutine("SpawnTornade1");
                Air1.SetActive(true);
                EtageA.SetActive(false);
            }
        }
        else if (vie == 1 && !changeTornade&&transform.position != pos3.transform.position)
        {
            transform.position = Vector2.MoveTowards(transform.position, new Vector2(pos3.transform.position.x, pos3.transform.position.y), 10f * Time.deltaTime);
            if (transform.position.y >= pos3.transform.position.y && Coroutine)
            {
                Coroutine=false;
                transform.rotation = Quaternion.Euler(0, 0, 0);
                StartCoroutine("SpawnTornade1");
                Air2.SetActive(true);
                EtageB.SetActive(false);
            }
        }
        else if (vie ==0)
        {
            Air3.SetActive(true);
            EtageC.SetActive(false);
            power.SetActive(true);
            Destroy(gameObject);
        }
    }

    IEnumerator SpawnTornade1()
    {        
            while (vie>=0)
            {
                    Instantiate(tornade, spawn.transform.position, Quaternion.Euler(0, 0, 0));
                    immuBoss = false;
                    yield return new WaitForSeconds(4f);
                    
            }
    }

    public void ChangePhase() // fait spawn le tornade qui ramene au spawn
    {
        Instantiate(tornade2,transform.position, Quaternion.Euler(0, 0, 0));
        Coroutine = true;
        changeTornade = false;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == ("Player") && 
        player.GetComponent<SpriteRenderer>().sprite == GameObject.Find("Inventory").GetComponent<InventoryScript>().transformationSprites[enclumeNumber])
        {
            StopCoroutine("SpawnTornade1");
            changeTornade = true;
            anim.SetTrigger("damage");
            Torn = GameObject.FindGameObjectsWithTag("Tornade");
            if (!immuBoss)
            {
                anim.SetTrigger("damage");
                vie--;
                immuBoss = true;
                for ( int i = 0; i < Torn.Length; i++)
                {
                    Destroy(Torn[i]);
                }
           }
        }
    }
}
