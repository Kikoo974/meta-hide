﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindPower : MonoBehaviour {

    private float speed = 2f;
    private Transform player;
    private Rigidbody2D p_rigidbody;
    private int FlyNumber, numberOfMove;
    private bool StopFollow;
    private Camera mainCamera;
    private void Start()
    {
        StopFollow=false;
        GameObject playeur= GameObject.Find("player");
        player = playeur.GetComponent<Transform>();
        p_rigidbody = playeur.GetComponent<Rigidbody2D>();
        Vector2 pos = player.localScale;
        mainCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
        //changer le direction de la tornade selon vers ou regarde le player
        if (player.rotation.y == 0)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(-speed, 0);
        }
        else
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(speed, 0);
        }

        if (GameObject.Find("plume"))
        {
            FlyNumber = GameObject.Find("plume").GetComponent<ItemScript>().itemNumber;
        }
    }
    

    private void Update()
    {
        //detruit la tornade quand celle-ci n'ai plus visible par la camera
        Vector2 screenPosition = mainCamera.WorldToViewportPoint (transform.position);
           if (screenPosition.x >1.05 || screenPosition.x < -0.05)
           {
                player.GetComponent<PlayerController>().canPower = true;
                Destroy (gameObject);
           }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Mur")
        {
            DeleteWind();
        }
        if (other.gameObject.tag == "MovingObject")
        {
            numberOfMove += 1;
        }
    }
    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.tag == "MovingObject" && numberOfMove==1)
        {
            
                other.transform.position = new Vector2(transform.position.x, other.transform.position.y);
            
        }
        else {
            //rien faire
        }
        if(other.gameObject.tag == "Player"&&player.GetComponent<SpriteRenderer>().sprite == GameObject.Find("Inventory").GetComponent<InventoryScript>().transformationSprites[FlyNumber]&&!StopFollow)
        {
            p_rigidbody.bodyType = RigidbodyType2D.Kinematic;
            player.transform.parent = transform;
            player.transform.position = new Vector3(transform.position.x, transform.position.y+0.25f);
            player.GetComponent<PlayerController>().canTransform = false;
        }
        else{
            p_rigidbody.bodyType = RigidbodyType2D.Dynamic;
            player.GetComponent<PlayerController>().canTransform = true;
            player.transform.parent = null;
        }
    }
    public void DeleteWind(){
        p_rigidbody.bodyType = RigidbodyType2D.Dynamic;
        player.transform.parent = null;
        StopFollow = true;
        player.GetComponent<PlayerController>().canPower = true;
        Destroy(this.gameObject);
    }

}
