﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LampeCN : MonoBehaviour {
    GameManager gameManager;
    private GameObject player;
    InventoryScript inventory;
    [SerializeField] bool resetScene= false,OnGround;
    [SerializeField] float x, y;
    [SerializeField] GameObject BlackScreen;
    void Start () {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        player = GameObject.FindGameObjectWithTag("Player");
        inventory = GameObject.Find("Inventory").GetComponent<InventoryScript>();
    }
	
	void Update () {
        
    }
    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player" && (!player.GetComponent<PlayerController>().hidden || GameObject.Find("Taunter")))
        {
            Ray();
            if (!OnGround)
            {
                if (!resetScene)
                {
                    if (inventory.pvPlayer > 1)
                    {
                        player.transform.position = new Vector3(x, y, player.transform.position.z);
                        inventory.DamageTakenByPlayer();
                        StartCoroutine("Black");
                    }
                    else
                        inventory.DamageTakenByPlayer();
                }
                else if (resetScene)
                {
                    inventory.GameOver();
                }
            }
            else
            {

            }
        }
    }
    void Ray()
    {
        string[] MaskNameList = new string[] { "Ground" };
        int mask = LayerMask.GetMask(MaskNameList);
        Vector3 posPlayer = player.transform.position;
        RaycastHit2D Ground = Physics2D.Raycast(this.gameObject.transform.position, posPlayer - transform.position,3, mask);
        OnGround = Ground;

        Debug.DrawRay(this.gameObject.transform.position, posPlayer-transform.position, Color.red);
    }
    IEnumerator Black()
    {
        player.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        BlackScreen.SetActive(true);
        player.GetComponent<PlayerController>().canMove = false;
        yield return new WaitForSeconds(1f);
        BlackScreen.SetActive(false);
        player.GetComponent<PlayerController>().canMove = true;

    }
}
