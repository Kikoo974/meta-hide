﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class triggerPorteFin : MonoBehaviour {
    [SerializeField] int total;
    [SerializeField] GameObject platform,trigger,textTrigger;
    private GameObject player;
    private int numberOn = 0;
    GameObject camera;

    void Start()
    {
        camera = GameObject.Find("Main Camera");
        player = GameObject.FindGameObjectWithTag("Player");
    }


    void Update()
    {


    }
    public void GetNumber()
    {
        numberOn += 1;
        if (numberOn >= total)
        {
            StartCoroutine("Camera");
        }
        else if (numberOn >= (total - 1))
        {
            platform.SetActive(true);
        }
    }
    IEnumerator Camera()
    {
        camera.GetComponent<CameraFollowerStory>().enabled = false;
        player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        player.GetComponent<PlayerController>().canMove = false;
        player.GetComponent<PlayerController>().canJump = false;
        camera.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, -10);
        yield return new WaitForSeconds(1.5f);
        Destroy(trigger);
        textTrigger.SetActive(false);
        yield return new WaitForSeconds(1f);
        camera.GetComponent<CameraFollowerStory>().enabled = true;
        player.GetComponent<PlayerController>().canMove = true;
        player.GetComponent<PlayerController>().canJump = true;

    }
}
